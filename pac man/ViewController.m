//
//  ViewController.m
//  pac man
//
//  Created by Timofei Harhun on 17.03.15.
//  Copyright (c) 2015 timofei. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) NSArray* imagerArrRight;
@property (strong, nonatomic) NSArray* imagerArrLeft;
@property (strong, nonatomic) NSArray* imagerArrTop;
@property (strong, nonatomic) NSArray* imagerArrBottom;
@property (strong, nonatomic) UIImage* imageClouse;
@property (strong, nonatomic) UIImage* foodImage;
@property (strong, nonatomic) UIImageView* pacMan;
@property (strong, nonatomic) NSMutableArray* food;
@property (assign, nonatomic) BOOL exit;

enum THCoordinate {
    THXCoordinate,
    THYCoordinate
};

typedef enum THCoordinate THCoordinate;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.exit = false;
    
    
    self.food = [NSMutableArray new];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    UIImageView* view = [[UIImageView alloc] initWithFrame:CGRectMake(40, 40, 20, 20)];
    view.backgroundColor = [UIColor clearColor];
    
    
    UIImage* imageLeftOpen = [UIImage imageNamed:@"leftOpen.png"];
    UIImage* imageLeftFullOpen = [UIImage imageNamed:@"leftFullOpen.png"];
    
    UIImage* imageRightOpen = [UIImage imageNamed:@"rightOpen.png"];
    UIImage* imageRightFullOpen = [UIImage imageNamed:@"rightFullOpen.png"];
    
    UIImage* imageBottomOpen = [UIImage imageNamed:@"bottomOpen.png"];
    UIImage* imageBottomFullOpen = [UIImage imageNamed:@"bottomFullOpen.png"];
    
    UIImage* imageTopOpen = [UIImage imageNamed:@"topOpen.png"];
    UIImage* imageTopFullOpen = [UIImage imageNamed:@"topFullOpen.png"];
    
    UIImage* imageClouse = [UIImage imageNamed:@"clouse.png"];
    self.imageClouse = imageClouse;
    
    UIImage* foodImage = [UIImage imageNamed:@"food.png"];
    self.foodImage = foodImage;
    
    self.imagerArrLeft = @[imageLeftFullOpen, imageClouse, imageLeftOpen];
    self.imagerArrRight = @[imageRightFullOpen, imageClouse, imageRightOpen];
    self.imagerArrBottom = @[imageBottomFullOpen, imageClouse, imageBottomOpen];
    self.imagerArrTop = @[imageTopFullOpen, imageClouse, imageTopOpen];
    
    self.pacMan = view;
    [self backgroundImage: self.imageClouse inView: self.pacMan];
    
    view.animationDuration = 0.3f;
    [self.view addSubview:view];
    self.view.multipleTouchEnabled = YES;
    [self.view bringSubviewToFront:self.pacMan];

}

#pragma mark Private Methods

- (void) backgroundImage:(UIImage*) image inView:(UIView*) view {
    
    UIImageView *background = [[UIImageView alloc] initWithImage: image];
    background.bounds = view.bounds;

    [view addSubview: background];
    [view sendSubviewToBack: background];

}



- (void) moveView: (UIImageView*) view inView:(UIView*) viewIn  {
    
    self.exit = true;
    for (UIView* subView in view.subviews) {
        [subView removeFromSuperview];
    }
    CGPoint coordPoint = viewIn.center;
    CGFloat speed = 70;
    CGFloat timeWidth = fabsf(view.center.x - coordPoint.x)/speed;
    CGFloat timeHeight = fabsf(view.center.y - coordPoint.y) /speed;
conformsToProtocol:
    
    [UIView animateKeyframesWithDuration: timeWidth
                                   delay:0
                                 options: UIViewAnimationCurveLinear
                              animations:^{
                                  [self chouseAnimationView: view
                                                    pointTo: coordPoint
                                                 coordinate: THXCoordinate ];
                                  
                              } completion:^(BOOL finished) {
                                  
                                  [UIView animateKeyframesWithDuration:timeHeight
                                                                 delay:0
                                                               options:UIViewAnimationCurveLinear
                                                            animations:^{
                                                                [self chouseAnimationView: view
                                                                                  pointTo: coordPoint
                                                                               coordinate: THYCoordinate ];
                                                                
                                                                
                                                            } completion:^(BOOL finished) {
                                                                
                                                                if(CGPointEqualToPoint( viewIn.center, view.center)){
                                                                    [viewIn removeFromSuperview];
                                                                    
                                                                }
                                                                [view stopAnimating];
                                                                [self backgroundImage:self.imageClouse inView:self.pacMan];
                                                                self.exit = false;
                                                                [self checkFood];
                                                                
                                                            }];
                                  
                              }
     ];

    
}

- (void) chouseAnimationView: (UIImageView*) view pointTo: (CGPoint) pointTo coordinate: (THCoordinate) coord {
    
    CGFloat pointView;
    CGFloat point;
    NSArray* arrOne = [NSMutableArray new];
    NSArray* arrTwo = [NSMutableArray new];
    CGPoint p;
    
    switch (coord) {
        case THXCoordinate:
            pointView = view.center.x;
            point = pointTo.x;
            arrOne = self.imagerArrRight;
            arrTwo = self.imagerArrLeft;
            p = CGPointMake(point, view.center.y);
            break;
        case THYCoordinate:
            pointView = view.center.y;
            point = pointTo.y;
            arrOne = self.imagerArrBottom;
            arrTwo = self.imagerArrTop;
            p = CGPointMake(view.center.x, point);
            break;
        default:
            break;
    }
    if (point != pointView) {
        if (point > pointView) {
            view.animationImages = arrOne;
            [view startAnimating];
            view.center = p;
        } else {
            view.animationImages = arrTwo;
            [view startAnimating];
            view.center = p;
            
        }
        
    }

    
}

- (UIView*) addFoodImage: (UIImage*) foodImage whithCoordinate: (CGPoint) point {
    
    UIImageView* view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    view.backgroundColor = [UIColor clearColor];
    
    view.center = point;
    
    [self backgroundImage:foodImage inView:view];
    
    [self.view addSubview:view];
    
    return  view;
    
}

- (void) checkFood {
    int i=0;
    if (self.food.count != 0) {
       
        __weak UIView* view = self.food[i];
        [self.food removeObjectAtIndex:i];
        [self moveView:self.pacMan inView:view];
        
       
    } ;
}

#pragma mark Touches

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    CGPoint pointTouch = [[touches anyObject] locationInView:self.view];
    
    UIView* view = [self addFoodImage: self.foodImage
       whithCoordinate: pointTouch];
    
    [self.food addObject:view];
    [self.view bringSubviewToFront:self.pacMan];
    if (!self.exit) {
        [self checkFood];
    }
    
}


- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    CGPoint pointTouch = [[touches anyObject] locationInView:self.view];
    
    UIView* view = [self addFoodImage: self.foodImage
                      whithCoordinate: pointTouch];
    [self.view bringSubviewToFront:self.pacMan];
    
    [self.food addObject:view];
    if (!self.exit) {
        [self checkFood];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
