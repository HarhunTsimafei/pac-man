//
//  main.m
//  pac man
//
//  Created by Timofei Harhun on 17.03.15.
//  Copyright (c) 2015 timofei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
