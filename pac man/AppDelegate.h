//
//  AppDelegate.h
//  pac man
//
//  Created by Timofei Harhun on 17.03.15.
//  Copyright (c) 2015 timofei. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

